#! /bin/bash
## This script is used to test the docker container once it is up and running

DOCKER_IMAGE_NAME=${1}
TAG=${2}
current_folder=${ROOT_FOLDER}/xprbuild/docker

if [[ -z "$DOCKER_IMAGE_NAME" ]]
then
	DOCKER_IMAGE_NAME=${COMPONENT_NAME}
fi
if [[ -z "$TAG" ]]
then
	TAG=${PROJECT_VERSION}
fi

echo "Checking syntax errors in Notebooks under app directory"
export PYTHONPATH=${ROOT_FOLDER}:${PYTHONPATH}
pytest --nbval-lax ${ROOT_FOLDER}/app/

## Add your test script here
cmd="docker run --rm --entrypoint=/bin/bash ${DOCKER_IMAGE_NAME}:${TAG} xprbuild/system/linux/test.sh"
echo "Run docker -> $cmd"
exec $cmd
