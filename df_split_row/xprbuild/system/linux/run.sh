#! /bin/bash
## This script is used to run the project. It shuold contain the script which will run the project
##
## DO NOT USE SUDO in the scripts. These scripts are run as sudo user
set -e

echo "Handling command-line arguments for papermill command"
counter=1
cmd_line_args=("--log-output" "-p enable_local_execution False")
for arg in $@
do
  if [ $arg == $1 ]
  then
    cmd_line_args+=("-p xpresso_run_name ${arg}")
  elif [ $arg == $2 ]
  then
    cmd_line_args+=("-p params_filename ${arg}")
  elif [ $arg == $3 ]
  then
    cmd_line_args+=("-p params_commit_id ${arg}")
  else
    cmd_line_args+=("-p cmd_line_variable_${counter} ${arg}")
    ((counter++))
  fi
done

# Run the application
export PYTHONPATH=${ROOT_FOLDER}:${PYTHONPATH}
echo "Starting the job"
papermill ${ROOT_FOLDER}/app/main.ipynb ${ROOT_FOLDER}/app/main.ipynb ${cmd_line_args[@]}
