"""
This is a flask test module. It list down the tests required for flask
app
"""

import os
import compileall

ROOT_FOLDER = os.environ.get("ROOT_FOLDER")
USER_APPLICATION_DIRECTORY = 'app'


def test_python_files_in_app_directory():
    """
        This test is to check any Syntax errors in all the
        python files generated under ROOT_FOLDER/app directory
    """
    error = compileall.compile_dir(os.path.join(ROOT_FOLDER, USER_APPLICATION_DIRECTORY), force=True)
    assert error is True
