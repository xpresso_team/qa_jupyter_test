"""Factory class for visualization"""
from enum import Enum
from xpresso.ai.core.data.visualization.plotly_visualization import \
    PlotlyVisualization
from xpresso.ai.core.data.visualization.seaborn_visualization import \
    SeabornVisualization
from xpresso.ai.core.logging.xpr_log import XprLogger


class VisualizationBackend(Enum):
    """
    Enum class that lists visualization libraries supported by
    Xpresso Visualization library
    """

    SEABORN = "seaborn"
    PLOTLY = "plotly"


class Visualization:
    """Factory to get visualization object"""

    @staticmethod
    def get_visualizer(dataset,
                       visualization_library=VisualizationBackend.SEABORN.value):
        """
            This method returns Visualization object of a specific visualization
            library
            Args:
                dataset(:obj StructuredDataset): Structured dataset object on
                which visualization to be performed
                visualization_library(:str): Visualization library to be used.
            Returns: Visualization object
        """
        if visualization_library.lower() == VisualizationBackend.SEABORN.value:
            ret_object = SeabornVisualization(dataset)
        elif visualization_library.lower() == VisualizationBackend.PLOTLY.value:
            ret_object = PlotlyVisualization(dataset)
        else:
            XprLogger().error(
                "Invalid visualization library. Using default library(seaborn)")
            ret_object = SeabornVisualization(dataset)
        return ret_object
