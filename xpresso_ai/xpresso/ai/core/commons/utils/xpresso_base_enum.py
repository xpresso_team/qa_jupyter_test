"""
    xpresso platform's base enum class
"""

__all__ = ["BaseEnum"]
__author__ = ["Shlok Chaudhari"]


import enum


class BaseEnum(enum.Enum):

    @classmethod
    def get(cls) -> list:
        return list(map(lambda var: var.value, cls))
