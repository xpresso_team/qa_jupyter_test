"""
    HTTP requests client for Model Monitoring
"""

__all__ = ["HTTPRequestClient"]
__author__ = ["Shlok Chaudhari"]


import urllib3
import requests
from json import JSONDecodeError

from xpresso.ai.core.model_monitoring.request_client.abstract_request_client import \
    AbstractRequestClient
from xpresso.ai.core.commons.exceptions.xpr_exceptions import ModelMonitoringServiceFailed
from xpresso.ai.core.commons.utils.constants import OUTCOME_FAILURE, OUTCOME, ERROR_CODE_KEY, \
    RESULTS_KEY, MESSAGE


class HTTPRequestClient(AbstractRequestClient):
    """
        This is class for implementing HTTP request client
        for Model Monitoring
    """

    urllib3.disable_warnings(urllib3.exceptions.SubjectAltNameWarning)
    urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

    def __init__(self, max_retry: int):
        self._max_retry = max_retry
        super(HTTPRequestClient, self).__init__()

    def send(self, method, url, **kwargs):
        """
            This method is used to send request to Model Monitoring
            API server. This method is written on top of requests
            library (requests.request() method) so all the kwargs
            valid for that method are valid here.
        Args:
            method(str): Method (POST/GET/PUT/DELETE)
            url(str): URL of the API server
            **kwargs: Keyword arguments required by requests.request()
             method
        Returns:
            response(dict)
        Raises:
            ModelMonitoringServiceFailed
        """
        response = None
        attempts = self._max_retry
        while attempts > 0:
            try:
                response = requests.request(method=method, url=url, **kwargs)
                break
            except Exception:
                self._logger.exception(
                    "Failed to send request to Model Monitoring. Retrying....")
                attempts -= 1
        if not response:
            self._logger.exception(
                "Failed to send request to Model Monitoring API server.")
            raise ModelMonitoringServiceFailed
        return self._process_response(response)

    def _process_response(self, response):
        """
            Processes response from model monitoring
            for all HTTP requests
        Args:
            response: response object for a HTTP request
        Returns:
             returns response object in case of success
             else raises an exception
        Raises:
            ModelMonitoringServiceFailed
        """
        error_code = None
        error_message = None
        try:
            if response.json()[OUTCOME] == OUTCOME_FAILURE:
                error_code = response.json()[ERROR_CODE_KEY]
                error_message = response.json()[RESULTS_KEY][MESSAGE]
        except (KeyError, JSONDecodeError, Exception):
            error_code = response.status_code
            error_message = response.text
        finally:
            if error_code and error_message:
                self._logger.exception(
                    f"Response status code: {error_code}\n"
                    f"Response text: {error_message}")
                raise ModelMonitoringServiceFailed(
                    "Request to Model Monitoring failed\n"
                    f"Response status code: {error_code}\n"
                    f"Response text: {error_message}")
            return response
